# yoga-space-be



## Getting started

### environment variables 

for DEV you can use the following

| name | value |
| ---- | ----- |
| PYTHONUNBUFFERED | 1 |
| ALLOWED_HOSTS | ['localhost', '127.0.0.1'] |
| CSRF_TRUSTED_ORIGINS | [] |
| DEBUG | 1 |
| DJANGO_SECRET_KEY | django-insecure--96)(rep7scqkrsrn#0#+b=e-mz#brz)kd60+8219%#ur__^9* |
| SITE_ID | 1 |
or more convenient:

PYTHONUNBUFFERED=1;ALLOWED_HOSTS=['localhost', '127.0.0.1'];CSRF_TRUSTED_ORIGINS=[];DEBUG=1;DJANGO_SECRET_KEY=django-insecure--96)(rep7scqkrsrn#0#+b=e-mz#brz)kd60+8219%#ur__^9*;SITE_ID=1

for other environments you have to make them yourself

### instaling google cloud
In case you want to use google cloud for hosting, install the cli here
https://cloud.google.com/sdk/docs/install

## Usage

### DEV
run manage.py runserver with the given environment variables

### ACC 
the acceptance script (push_acc.sh) is optimized for google cloud run, so you have to be logged in with google cloud
run the script after changing the needed parameters

### PRD
coming soon!