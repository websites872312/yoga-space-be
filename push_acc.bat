docker build -t yoga-space -f Dockerfile.acc .
docker tag yoga-space gcr.io/yoga-space-be/yoga-space-acc
docker push gcr.io/yoga-space-be/yoga-space-acc

gcloud run deploy yoga-space-acc --image gcr.io/yoga-space-be/yoga-space-acc --region europe-west1