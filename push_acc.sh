#!/bin/sh

python manage.py collectstatic --noinput

docker build -t yoga-space-acc -f Dockerfile.acc .
docker tag yoga-space-acc gcr.io/yoga-space-be/yoga-space-acc
docker push gcr.io/yoga-space-be/yoga-space-acc

gcloud run deploy yoga-space-acc --image gcr.io/yoga-space-be/yoga-space-acc --region europe-west1
