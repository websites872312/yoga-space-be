from django import forms
from django.contrib.auth.models import User

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV3


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)
    captcha = ReCaptchaField(widget=ReCaptchaV3)


class AccountForm(forms.ModelForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)

    class Meta:
        model = User
        fields = ['first_name', 'last_name']


class DeleteAccountForm(forms.Form):
    confirmation = forms.CharField(max_length=100, required=False)
    i_understand = forms.BooleanField(required=False)

    def clean(self):

        if self.data.get('confirmation') != 'delete_my_account':
            self.add_error("confirmation", "Type 'delete_my_account' in the box.")

        if not self.data.get('i_understand'):
            self.add_error("i_understand", "Check the 'I understand' box.")

        super().clean()