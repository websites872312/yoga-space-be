from django.contrib import admin
from .models import Course, Lesson, Student, Address

admin.site.register(Address)
admin.site.register(Course)
admin.site.register(Lesson)
admin.site.register(Student)
