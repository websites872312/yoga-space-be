import datetime

from django.contrib import sitemaps
from django.urls import reverse

from .models import Lesson


class StaticViewSitemap(sitemaps.Sitemap):
    changefreq = "monthly"

    SITE = {
        'index': {
            'priority': 1.0
        },
        'schedule': {
            'priority': 1.0
        },
        'classes': {
            'priority': 0.8
        },
        'pricing': {
            'priority': 0.8
        },
        'about': {
            'priority': 0.5
        },
        'contact': {
            'priority': 0.5
        },
        'policy': {
            'priority': 0.1
        },
        'account_signup': {
            'priority': 0.5
        }
    }

    def items(self):
        return list(self.SITE.keys())

    def location(self, item):
        return reverse(item)

    def priority(self, item):
        return self.SITE[item].get('priority', 0.5)


class ClassesSitemap(sitemaps.Sitemap):
    changefreq = "weekly"

    CLASSES = {
        'Ashtanga': {
            'priority': 0.8,
            'loc': '/classes/#ashtanga',
        },
        'Slow': {
            'priority': 0.8,
            'loc': '/classes/#slow',
        },
        'Vinyasa': {
            'priority': 0.8,
            'loc': '/classes/#vinyasa',
        },
        'Mysore': {
            'priority': 0.8,
            'loc': '/classes/#mysore',
        },
        'Private': {
            'priority': 0.5,
            'loc': '/classes/#private',
        },
        'Business': {
            'priority': 0.5,
            'loc': '/classes/#business',
        },
    }

    def items(self):
        return list(self.CLASSES.keys())

    def priority(self, item):
        return self.CLASSES[item].get('priority', 0.5)

    def location(self, item):
        return self.CLASSES[item].get('loc', '/')


class LessonSitemap(sitemaps.Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        today = datetime.date.today()
        return Lesson.objects.filter(date__gte=today, date__lte=today+datetime.timedelta(days=7))

    def lastmod(self, obj):
        return obj.date

    def location(self, item):
        return '/schedule/'


sitemaps = {
    'static': StaticViewSitemap,
    # 'classes': ClassesSitemap,
    # 'lessons': LessonSitemap,
}