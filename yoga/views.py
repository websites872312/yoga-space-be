from io import BytesIO
from pathlib import Path
import json
from zipfile import ZipFile, ZIP_DEFLATED
import datetime

from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.decorators import user_passes_test
from django.db import connection
from django.http import HttpRequest
from django.http.response import FileResponse, JsonResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.utils import timezone
from django.views.decorators.http import require_http_methods
from django.core.mail import EmailMessage
from django.conf import settings
from django.urls import reverse
from meta.views import Meta

from .forms import ContactForm, AccountForm, DeleteAccountForm
from .models import Lesson


def index(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('index'),
            title='Yoga Space - Yoga in Genk',
            description='Yoga for everyone',
            keywords=['yoga', 'genk', 'limburg', 'ashtanga', 'vinyasa', 'mysore', 'private', 'business',],
        )
    }
    return render(request, 'home.html', ctx)


def schedule(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('schedule'),
            title='Our Schedule',
            description='The schedule of our classes',
            keywords=['yoga', 'genk', 'limburg', 'classes', 'schedule'],
        )
    }
    return render(request, 'schedule.html', ctx)


def classes(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('classes'),
            title='Our Classes',
            description='The schedule of our classes',
            keywords=['yoga', 'genk', 'limburg', 'classes', 'ashtanga', 'vinyasa', 'mysore', 'private', 'business',],
        )
    }
    return render(request, 'classes.html', ctx)


def pricing(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('pricing'),
            title='Our Prices',
            description='The prices of our classes',
            keywords=['yoga', 'genk', 'limburg', 'pricing',],
        )
    }
    return render(request, 'pricing.html', ctx)


def get_lessons(request, year, month):
    # request.is_ajax() is deprecated since django 3.1
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_ajax:
        if request.method == 'GET':
            # get all lessons from that month
            queried_lessons = Lesson.objects.filter(
                date__year=int(year)
            ).filter(
                date__month=int(month)+1
            ).order_by(
                'date', 'course__time'
            )
            try:
                student = request.user.student
            except AttributeError:
                student = None

            return JsonResponse({'lessons': [x.as_dict(student=student) for x in queried_lessons if x.state != 'Hidden']})
        return JsonResponse({'status': 'Invalid request'}, status=400)
    else:
        return HttpResponseBadRequest('Invalid request')


def book_lesson(request, id):

    if isinstance(request.user, AnonymousUser):
        return JsonResponse({'status': 'Invalid request', 'redirect_url': reverse('account_login')}, status=400)
    # request.is_ajax() is deprecated since django 3.1
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_ajax:
        if request.method == 'PUT':
            # retrieving the student
            student = request.user.student

            # retrieving the lesson
            lesson = Lesson.objects.get(id=id)

            return student.register(lesson)

        return JsonResponse({'status': 'Invalid request'}, status=400)
    else:
        return HttpResponseBadRequest('Invalid request')


def about(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('about'),
            title='About Us',
            description='Information about who we are',
            keywords=['yoga', 'genk', 'limburg', 'about',],
        )
    }
    return render(request, 'about.html', ctx)


def contact(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('contact'),
            title='Contact Us',
            description='If you have a question, let us know',
            keywords=['yoga', 'genk', 'limburg', 'contact',],
        ),
        'form': ContactForm(initial={'sender': '', 'subject': '', 'message': ''})
    }
    return render(request, 'contact.html', ctx)


def policy(request):
    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('policy'),
            title='Our Privacy Policy',
            description='Your privacy, our concern',
            keywords=['yoga', 'genk', 'limburg', 'privacy',],
        )
    }
    return render(request, 'policy.html', ctx)


def send_form(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ContactForm(request.POST)
        # check whether it's valid:
        if form.is_valid():

            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']

            recipients = [settings.DEFAULT_FROM_EMAIL]
            if cc_myself:
                recipients.append(sender)
            body = f"""
You have a new message from : {sender}
            
Body:
{message}
            """

            email = EmailMessage(
                subject,
                body,
                settings.DEFAULT_FROM_EMAIL,
                recipients,
                reply_to=[sender],
            )
            email.send()
            # send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect('/contact/')
        else:
            print('derp')

    ctx = {
        'meta': Meta(
            use_sites=True,
            use_title_tag=True,
            use_og=True,
            site_name=reverse('index'),
            url=reverse('contact'),
            title='Contact Us',
            description='If you have a question, let us know',
            keywords=['yoga', 'genk', 'limburg', 'contact',],
        ),
        'form': form,
    }
    return render(request, 'contact.html', ctx)


def account_details(request):
    if isinstance(request.user, AnonymousUser):
        # messages.add_message(request, messages.INFO, "Session expired, logged out user.")
        return render(request, 'home.html', {})

    context = {
        'name': f'{request.user.first_name} {request.user.last_name}' if bool(request.user.first_name) else f'{request.user.username}',
        'credits': request.user.student.lesson_credits,
    }
    return render(request, 'account/overview.html', context)


def account_edit(request):
    if isinstance(request.user, AnonymousUser):
        # messages.add_message(request, messages.INFO, "Session expired, logged out user.")
        return render(request, 'home.html', {})

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # check if the delete form is valid
        delete_form = DeleteAccountForm(request.POST)
        if request.POST['action'] == 'edit':

            # check if the account form is valid
            account_form = AccountForm(request.POST)
            # check whether it's valid:
            if account_form.is_valid():


                request.user.first_name = account_form.cleaned_data['first_name']
                request.user.last_name = account_form.cleaned_data['last_name']
                request.user.save()

                return HttpResponseRedirect(reverse('account_overview'))
        elif request.POST['action'] == 'delete':
            if delete_form.is_valid():
                request.user.delete()
                messages.add_message(request, messages.INFO, "Account successfully deleted.")
                logout(request)
                return HttpResponseRedirect(reverse('index'))

            else:
                for error in delete_form.errors.values():
                    messages.add_message(request, messages.INFO, error)
                return HttpResponseRedirect(reverse('account_edit'))

        else:
            print('derp')

    context = {
        'username': request.user.username,
        'first_name': request.user.first_name,
        'last_name': request.user.last_name,
        'email': request.user.email,
    }
    return render(request, 'account/edit.html', context)


def account_lessons(request):
    if isinstance(request.user, AnonymousUser):
        # messages.add_message(request, messages.INFO, "Session expired, logged out user.")
        return HttpResponseBadRequest('Invalid request')

    # request.is_ajax() is deprecated since django 3.1
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_ajax:
        if request.method == 'GET':
            today = datetime.date.today()
            lessons = list(request.user.student.booked_classes.all()) + list(
                request.user.student.reserved_classes.all())
            lessons = [x for x in lessons if x.date + datetime.timedelta(days=7) >= today]

            future_lessons = [
                Lesson.objects.filter(date=x.date + datetime.timedelta(days=7), course=x.course).first() for x
                in lessons]

            lessons = lessons + [x for x in future_lessons if x not in lessons and x is not None]

            week_lessons = Lesson.objects.filter(date__gte=today, date__lte=today+datetime.timedelta(days=7))
            lessons = lessons + [x for x in week_lessons if x not in lessons and x is not None]

            lessons = sorted(lessons, key=lambda x: datetime.datetime.combine(x.date, x.course.time))

            return JsonResponse({'lessons': [x.as_dict(student=request.user.student) for x in lessons if x.state != 'Hidden']})
        return JsonResponse({'status': 'Invalid request'}, status=400)
    else:
        return HttpResponseBadRequest('Invalid request')


@user_passes_test(lambda u: u.is_superuser)
@require_http_methods(["GET"])
def export_database_view(request: HttpRequest) -> FileResponse:
    """Returns the database as a zipped file to download"""

    # Get the path to the database
    db_path = Path(connection.settings_dict["NAME"])
    timestamp = timezone.localtime().strftime("%Y-%m-%d-%H-%M-%S")
    zip_filename = f"db {timestamp}.zip"

    # Use BytesIO as an in memory zip file for the database
    mem_zip = BytesIO()
    with ZipFile(mem_zip, mode="w", compression=ZIP_DEFLATED) as zf:
        zf.write(db_path, 'db.sqlite3')  # write db file into zip file
    mem_zip.seek(0)

    return FileResponse(mem_zip, as_attachment=True, filename=zip_filename)
