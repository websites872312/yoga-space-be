from django.contrib import admin
from django.urls import path, include
from django.contrib.sitemaps import views as sitemap_views

from . import views
from .sitemaps import sitemaps

urlpatterns = [
    path('', views.index, name='index'),
    path('lessons/<year>/<month>/', views.get_lessons, name='lesson'),
    path('lessons/book/<int:id>', views.book_lesson, name='book_lesson'),
    path('schedule/', views.schedule, name='schedule'),
    path('classes/', views.classes, name='classes'),
    path('pricing/', views.pricing, name='pricing'),
    path('about/', views.about, name='about'),
    # path('blog/', views.blog, name='blog'),
    path('contact/', views.contact, name='contact'),
    path('contact/send/', views.send_form, name='send_form'),
    path('policy/', views.policy, name='policy'),
    path('admin/', admin.site.urls),
    path('export_db/', views.export_database_view, name='export_db'),
    path('accounts/', include('allauth.urls'), name='login'),
    path('accounts/lessons/', views.account_lessons, name='account_lessons'),
    path('accounts/edit/', views.account_edit, name='account_edit'),
    path('accounts/overview/', views.account_details, name='account_overview'),
    path(
        "sitemap.xml",
        sitemap_views.sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
]