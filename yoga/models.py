import datetime

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http.response import JsonResponse


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    lesson_credits = models.IntegerField(default=0)
    # workshops = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return f'{self.user.email}: credits left {self.lesson_credits}'

    def register(self, lesson):
        if not isinstance(lesson, Lesson):
            return JsonResponse({'message': 'not a lesson'}, status=406)

        # is it too late?
        timedelta = datetime.datetime.combine(lesson.date, lesson.course.time) - datetime.datetime.now()
        if timedelta <= datetime.timedelta(hours=1):
            return JsonResponse({'message': 'to late', 'lesson': lesson.pk}, status=405)

        if self in lesson.booked.all():
            # unbooking
            lesson.booked.remove(self)
            self.lesson_credits += 1
            self.save()
            lesson.save()
            return JsonResponse({'message': 'success'}, status=200)
        elif self in lesson.reserved.all():
            # we are unreserving
            lesson.reserved.remove(self)
            lesson.save()
            return JsonResponse({'message': 'success'}, status=200)
        else:
            # we are booking
            # if student.lesson_credits == 0:
            #     return JsonResponse({'message': 'no credits left'}, status=402)

            if (lesson.booked.count() + lesson.reserved.count()) >= lesson.course.max_students:
                return JsonResponse({'message': 'class full'}, status=405)

            if lesson.booked.contains(self) or lesson.reserved.contains(self):
                return JsonResponse({'message': 'already booked'}, status=405)

            if self.lesson_credits > 0:
                lesson.booked.add(self)
                self.lesson_credits -= 1
            else:
                lesson.reserved.add(self)
            self.save()
            lesson.save()
            return JsonResponse({'message': 'success'}, status=200)


@receiver(post_save, sender=User)
def create_user_student(sender, instance, created, **kwargs):
    if created:
        Student.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_student(sender, instance, **kwargs):
    instance.student.save()


class Address(models.Model):
    name = models.CharField("Full name", max_length=1024,)
    address = models.CharField("Address line", max_length=1024,)
    zip_code = models.CharField("ZIP / Postal code", max_length=12,)
    city = models.CharField("City", max_length=1024,)
    country = models.CharField("Country", max_length=64, default='België')
    link = models.CharField("maps_link", max_length=256, default='')

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"

    def __str__(self):
        return f"{self.name}: {self.address} - {self.zip_code}, {self.city}"


class Course(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=256, default='')
    start_date = models.DateField()
    end_date = models.DateField()
    time = models.TimeField()
    duration = models.FloatField()
    max_students = models.IntegerField()
    location = models.ForeignKey(Address, on_delete=models.CASCADE)
    image = models.CharField(max_length=100)

    def __str__(self):
        return f"({self.pk}) {self.name} | {self.start_date} to {self.end_date} ({self.duration}h - {self.max_students} students)"

    def save(self, *args, **kwargs):

        super(Course, self).save(*args, **kwargs)

        for delta in range(0, (self.end_date - self.start_date).days+1, 7):

            try:
                lesson = self.lesson_set.get(date=self.start_date+datetime.timedelta(days=delta))
            except Lesson.DoesNotExist:
                lesson = Lesson.objects.create(
                    course=self,
                    date=self.start_date+datetime.timedelta(days=delta),
                )
            lesson.save()


class Lesson(models.Model):
    STATES = (
        ('A', 'Active'),
        ('C', 'Cancelled'),
        ('H', 'Hidden'),
    )
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    date = models.DateField()
    booked = models.ManyToManyField(Student, default=[], related_name='booked_classes')
    reserved = models.ManyToManyField(Student, default=[], related_name='reserved_classes')
    confirmed = models.ManyToManyField(Student, default=[], related_name='confirmed_classes')
    state = models.CharField(max_length=1, choices=STATES, default='A')

    def __str__(self):
        return f"({self.pk}) {self.date} {self.course.time}: {self.course.name} ({self.course.duration}h - ({self.booked.count()}+{self.reserved.count()})/{self.course.max_students})"

    def as_dict(self, student=None):
        now = datetime.datetime.now() + datetime.timedelta(hours=1)
        result = {
            'id': self.pk,
            'name': self.course.name,
            'description': self.course.description,
            'state': self.state,
            # 'time': time.mktime(datetime.datetime.combine(self.date, self.course.time).timetuple())*1000,
            'year': self.date.year,
            'month': self.date.month,
            'day': self.date.day,
            'hour': self.course.time.hour,
            'minute': self.course.time.minute,
            'duration': self.course.duration,
            'nstudents':  self.booked.count()+self.reserved.count()+1,
            'max_students': self.course.max_students,
            'booking': 'completed' if now > datetime.datetime.combine(self.date, self.course.time) else 'none' if student is None else 'reserved' if self.reserved.contains(student) else 'booked' if self.booked.contains(student) else 'none',
            'location': str(self.course.location),
            'maps_link': self.course.location.link,
            'image': self.course.image,
            # 'reserved': self.reserved.contains(student) if student is not None else False,
        }
        return result


@receiver(post_save, sender=Lesson)
def remove_from_inventory(sender, instance, created, **kwargs):
    if instance.state == 'C' and not created:
        for student in set(instance.booked.all()).union(instance.reserved.all()):
            student.register(instance)
