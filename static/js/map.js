// Initialize and add the map
function initMap() {
  // The location of Uluru
  const yogaspace = { lat: 50.964206, lng: 5.442168 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 14,
    center: yogaspace,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: yogaspace,
    map: map,
  });
}

window.initMap = initMap;