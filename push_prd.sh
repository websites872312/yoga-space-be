#!/bin/sh

python manage.py collectstatic --noinput

docker build -t yoga-space -f Dockerfile.prd .
docker tag yoga-space gcr.io/yoga-space-be/yoga-space
docker push gcr.io/yoga-space-be/yoga-space

gcloud run deploy yoga-space --image gcr.io/yoga-space-be/yoga-space --region europe-west1
